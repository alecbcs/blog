---
title: "This Post Is Going to Suck"
date: 2024-06-23T17:55:17-07:00
draft: false
toc: false
images:
tags:
  - personal
  - blog
  - writing
---

**This post is going to suck.**

I know probably not the hook my college professors would have suggested for an
introductory piece of writing, but let me explain. I've suffered with perfectionism
my whole life. In art classes I'd either sit frozen at a blank canvas or spend
twice as long as everyone else tweaking a design I should have finished hours ago.

I'm similarly obsessive about my own writing. No matter whether it's a text to a
friend or a technical whitepaper, I can't help myself pressing backspace and tying
again. I seriously think I might be the only person who regularly makes use of
the edit feature in text messages outside of fixing devastating autocorrect
failures. (Like if you accidentally tell your mother that you "loathe" her
instead of that you "love" her -- I recommend bringing flowers, even if you
do edit that one.)

*So this post is going to suck.*

I don't mean I'm going to intentionally write this post badly, but when compared
to the perfect and elusive blog post in my head, this one is going to suck. And I
think I'm okay with that. I'd rather finally get some ideas down on paper than
sit here frozen any longer.

If you've struggled with perfectionism in your life, I'd be interested to hear
how you've managed to create in spite of that little voice in your head always
asking for more time, another draft, or just telling you to quit and move on
to another project. (Email me @ hi at alecbcs.com.) If you haven't concurred
your perfectionism, I encourage you to start doing things badly. Hit submit
instead of backspace. What's the worst thing that could happen?
